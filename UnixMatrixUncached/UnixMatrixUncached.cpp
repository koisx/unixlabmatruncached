#include "stdafx.h"
#include <iostream>
#include <thread>
#include <ctime>

using namespace std;

const int NMAX = 1000;
const int PREDEFINED_MATR_SIZE = 10;

volatile int A[NMAX][NMAX];
volatile int B[NMAX][NMAX];
volatile int C[NMAX][NMAX];//resulting matrix

void multVV(int i, int j, int s) {
	C[i][j] = 0;
	for (int k = 0; k < s; k++) {
		C[i][j] += A[i][k] * B[k][j];
	}
	std::cout << "C[" << i << "," << j << "] = " << C[i][j] << std::endl;
}

void genmatrix(int r, int c, volatile int M[][NMAX]) {
	srand(time(NULL));
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			M[i][j] = rand() % 200;
		}
	}
}

int main() {
	genmatrix(PREDEFINED_MATR_SIZE, PREDEFINED_MATR_SIZE, A);
	genmatrix(PREDEFINED_MATR_SIZE, PREDEFINED_MATR_SIZE, B);

	cout << "A matrix generated:" << endl;
	for (int i = 0; i < PREDEFINED_MATR_SIZE; i++) {
		for (int j = 0; j < PREDEFINED_MATR_SIZE; j++) {
			cout << A[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl;

	cout << "B matrix generated:" << endl;
	for (int i = 0; i < PREDEFINED_MATR_SIZE; i++) {
		for (int j = 0; j < PREDEFINED_MATR_SIZE; j++) {
			cout << B[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl;

	clock_t begin = clock();
	std::thread * th[PREDEFINED_MATR_SIZE][PREDEFINED_MATR_SIZE];
	cout << "Demonstration of calculation order:" << endl;
	for (int i = 0; i < PREDEFINED_MATR_SIZE; i++) {
		for (int j = 0; j < PREDEFINED_MATR_SIZE; j++) {
			th[i][j] = new std::thread(multVV, i, j, PREDEFINED_MATR_SIZE);
		}
	}

	//waiting for all threads to terminate
	for (int i = 0; i < PREDEFINED_MATR_SIZE; i++) {
		for (int j = 0; j < PREDEFINED_MATR_SIZE; j++) {
			(*th[i][j]).join();
		}
	}
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

	cout << endl;

	cout << "C matrix:" << endl;
	for (int i = 0; i < PREDEFINED_MATR_SIZE; i++) {
		for (int j = 0; j < PREDEFINED_MATR_SIZE; j++) {
			cout << C[i][j] << " ";
		}
		cout << endl;
	}
	cout << "Time:" << elapsed_secs;

	system("pause");
}

